@snap[north span-100]
## Git, Bitbucket, Source tree..
@snapend

@snap[south-west]
![](assets/img/git-logo.jpg)
@snapend

@snap[south-east]
![](assets/img/Bitbucket.jpg)
@snapend

---

@snap[north]
## Git
@snapend

@snap[west span-30]
@snap[text-08]
@ul
- Distributed version control system
- Created by Linus Torvalds
@ulend
@snapend
@snapend

@snap[east span-70]
![](assets/img/gitlocal.png)
@snapend

---
@snap[north-west]
### Github and Bitbucket
@snapend

@snap[west span-30]
Remote code repositories for collaboration
@snapend

@snap[east span-70]
![](assets/img/bb&gh.png)
@snapend

---

#### **Public repository**: Visible to anyone. Others (contributors) can fork and request for a merge using pull requests
#### **Private repository**: Only owners and collaborators can view or contribute to a private repository. This can be configured in repository settings

---

### Git commands

![](assets/img/gc1.png)

---
@snap[north]
### Creating repositories
@snapend

@snap[west sidebar]
@ul
- git init
- git clone
@ulend
@snapend

---
@snap[north]
### Syncing repositories
@snapend

@snap[west sidebar]
@ul
- git remote add <remote repo url>
- git pull
- git push
@ulend
@snapend

---
@snap[north-west]
### Making changes
@snapend

@snap[west span-30]
@ul[spaced]
- git status
- git add
- git commit
- git log
@ulend
@snapend

@snap[east span-70]
![](assets/img/makingchanges.png)
@snapend

---

@snap[north-west]
### Parallel development
@snapend

@snap[west span-30]
@ul[spaced]
- git branch
- git checkout
- git merge
- git diff
- git blame
@ulend
@snapend

@snap[east span-70]
![](assets/img/merge.png)
@snapend

---

![](assets/img/synopsis.png)

---

@snap[north]
### Patching
@snapend

@snap[west sidebar]
@ul
- git format-patch
- git apply
@ulend
@snapend

---

@snap[north]
### Pull requests
@snapend

@snap[text-08]
Pull requests let you tell others about changes you've pushed to a branch in a repository on GitHub. Can be used for reviews.
@snapend


---

@snap[north]
### Forking
@snapend

@snap[text-08]
Forking is a server side copy of a repository.
<br />
A great example of using forks to propose changes is for bug fixes.
<br />
<br />
@ul
- Fork the repository.
- Make the fix.
- Submit a pull request to the project owner.
@ulend

@snapend

---
@snap[north span-100]
## Sourcetree
@snapend

---
@snap[north span-35]
### Eclipse Plugin Egit

@snap[text-07]
Install new software - download.eclipse.org/egit/updates 
@snapend
@snapend

@snap[south span-65]
![](assets/img/egit.png)
@snapend

---
@snap[north span-100]
### Try out git
@snapend

git-school.github.io/visualizing-git
